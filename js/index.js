/*---Теоретичне завдання---
1. Чому для роботи з input не рекомендується використовувати клавіатуру?
Використання лише клавіатури - може створити проблеми з доступністю для деяких користувачів,
які покладаються на допоміжні технології (такі як програми зчитування з екрана; труднощі з
навігацією та взаємодією з веб-сайтом, який покладається виключно на введення з клавіатури)



---Практичне завдання---*/

const btnEl = document.querySelectorAll('.btn');

document.addEventListener("keydown", (event) => {
    console.log(event.key);
    btnEl.forEach((btnEl) => {
    if (event.key.toLowerCase() === btnEl.textContent.toLowerCase()) {
       btnEl.style.background = "blue";
    } else {
        btnEl.style.background = "black";
    }
})
   
});


















/*const btn = ["L", "S", "Enter", "E", "O", "N", "Z"];
let pressed = "";

window.addEventListener("keydown", e => {
    const key = e.key.toLowerCase();
    if(btn.includes(key)) {
        const btn = document.getElementById(key);
        btn.style.backgroundColor = "blue";
        if (pressed && pressed !== key) {
            const prevBtn = document.getElementById(pressed);
            prevBtn.style.backgroundColor = "black";
        }
        pressed = key;
    }
});*/





